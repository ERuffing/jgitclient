import org.gradle.kotlin.dsl.execution.ProgramText.Companion.from
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.openjfx.gradle.*

plugins {
    kotlin("jvm") version "1.8.10"
    application
    id("org.openjfx.javafxplugin") version "0.0.11"
    kotlin("plugin.serialization") version "1.8.10"
    id("org.beryx.runtime") version "1.12.5"
}
version = "1.0-SNAPSHOT"

val tornadofx_version: String by rootProject
val compileKotlin: KotlinCompile by tasks
val compileJava: JavaCompile by tasks

repositories {
    mavenCentral()
    maven {
        url = uri("https://github.com/javaterminal/terminalfx/raw/master/releases")
    }
    maven {
        url = uri("https://packages.jetbrains.team/maven/p/ij/intellij-dependencies")
    }
    maven {
        url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
    }
    maven {
        url = uri("https://repo1.maven.org/maven2/")
    }
}

application {
    mainClass.set("com.ethanruffing.ejgit.gui.EJGitKt")
    applicationDefaultJvmArgs = listOf(
        "--add-opens",
        "java.base/jdk.internal.misc=ALL-UNNAMED",
        "--add-opens",
        "java.base/java.nio=ALL-UNNAMED",
        "--add-opens",
        "java.base/java.security=ALL-UNNAMED",
        "--add-opens",
        "jdk.unsupported/sun.misc=ALL-UNNAMED",
        "--add-opens",
        "java.base/sun.security.action=ALL-UNNAMED",
        "--add-opens",
        "jdk.naming.rmi/com.sun.jndi.rmi.registry=ALL-UNNAMED",
        "--add-opens",
        "java.base/sun.net=ALL-UNNAMED",
        "--add-opens",
        "java.base/java.lang=ALL-UNNAMED",
        "--add-opens",
        "java.base/java.lang.reflect=ALL-UNNAMED"
    )
}

javafx {
    version = "19"
    modules = arrayOf( "javafx.controls", "javafx.fxml", "javafx.web", "javafx.swing", "javafx.graphics").toMutableList()
}

val javaFXOptions = the<JavaFXOptions>()
dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("no.tornado:tornadofx:$tornadofx_version")
    testImplementation(kotlin("test-junit"))
    implementation("com.ethanruffing.preferenceabstraction:preference-abstraction:0.2.3")
    implementation("commons-io:commons-io:latest.release")
    implementation("commons-lang:commons-lang:latest.release")
    implementation("com.google.guava:guava:latest.release")
    implementation("org.eclipse.jgit:org.eclipse.jgit:latest.release")
//    implementation("org.slf4j:slf4j-simple:latest.release")
    implementation("org.slf4j:slf4j-log4j12:latest.release")
    implementation("junit:junit:latest.release")
    implementation("org.jfxtras:jmetro:latest.release")
    implementation("org.kordamp.bootstrapfx:bootstrapfx-core:latest.release")
    implementation("com.kodedu.terminalfx:terminalfx:latest.release")
    JavaFXPlatform.values().forEach { platform ->
        val cfg = configurations.create("javafx_" + platform.classifier)
        JavaFXModule.getJavaFXModules(javaFXOptions.modules).forEach { m ->
            project.getDependencies().add(
                cfg.name,
                String.format("org.openjfx:%s", m.getArtifactName())
            );
        }
    }
}

tasks {
    compileKotlin {
    }
    compileTestKotlin {
    }
}

runtime {
    imageZip.set(project.file("${project.buildDir}/image-zip/EJGit.zip"))
    options.set(listOf("--strip-debug", "--compress", "2", "--no-header-files", "--no-man-pages"))
    modules.set(
        listOf(
            "java.desktop",
            "jdk.unsupported",
            "java.scripting",
            "java.logging",
            "java.xml",
            "java.desktop",
            "java.prefs",
            "jdk.jfr",
            "jdk.unsupported.desktop",
            "java.datatransfer",
            "java.scripting",
            "java.xml",
            "jdk.jsobject",
            "jdk.xml.dom",
            "jdk.unsupported",
            "java.logging",
            "java.security.jgss",
            "java.sql",
            "java.net.http"
        )
    )

//    targetPlatform("linux", System.getenv("JDK_LINUX_HOME"))
//    targetPlatform("mac", System.getenv("JDK_MAC_HOME"))
//    targetPlatform("win", System.getenv("JDK_WIN_HOME"))
    jpackage {
        installerOptions.addAll(listOf("--app-version", version as String))
        if (org.gradle.internal.os.OperatingSystem.current().isWindows) {
            imageOptions.addAll(
                arrayListOf(
                    "--icon",
                    "${projectDir}/src/main/resources/com/ethanruffing/ejgit/EJGitIcon.ico"
                )
            )
            installerOptions.add("--win-menu")
        } else {
            imageOptions.addAll(
                arrayListOf(
                    "--icon",
//                    "${projectDir}/src/main/resources/com/ethanruffing/ejgit/EJGitIconIcon.ico"
                    "${projectDir}/src/main/resources/com/ethanruffing/ejgit/EJGitIcon_256.png"
                )
            )
            if (org.gradle.internal.os.OperatingSystem.current().isLinux) {
                installerOptions.addAll(
                    listOf(
                        "--type",
                        "deb",
                        "--linux-deb-maintainer",
                        "ethan@eruffing.com",
                        "--linux-shortcut"
                    )
                )
            }
            if (org.gradle.internal.os.OperatingSystem.current().isMacOsX) {
                installerOptions.addAll(
                    listOf(
                        "--type",
                        "dmg",
                        "--icon",
//                    "${projectDir}/src/main/resources/com/ethanruffing/ejgit/EJGitIcon.ico"
                        "${projectDir}/src/main/resources/com/ethanruffing/ejgit/EJGitIcon_256.png",
                        "--mac-package-name",
                        "EJGit",
                        "--vendor",
                        "Ethan Ruffing",
                        "--copyright",
                        "Copyright 2023 Ethan Ruffing"

                    )
                )
            }
        }
    }
}

tasks.withType(CreateStartScripts::class).forEach { script ->
    script.doFirst {
        script.classpath = files("lib/*")
    }
}

tasks["runtime"].doLast {
    JavaFXPlatform.values().forEach { platform ->
        val cfg = configurations["javafx_" + platform.classifier]
        cfg.resolvedConfiguration.files.forEach { f ->
            copy {
                from(f)
                into("build/image/ejgit-${platform.classifier}/lib")
            }
        }
    }
}