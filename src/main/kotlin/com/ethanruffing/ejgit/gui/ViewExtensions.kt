package com.ethanruffing.ejgit.gui

import javafx.css.Styleable
import javafx.scene.Parent
import tornadofx.addClass
import tornadofx.getChildList
import kotlin.reflect.KClass


public fun <T : Styleable> Parent.applyClassRecursively(type: KClass<T>, s: String) {
    try {
        var childList = getChildList()
        if (childList == null || childList!!.isEmpty()) return;

        childList!!.filter { type.isInstance(it) }.addClass(s);
        for (child in childList!!.filterIsInstance<Parent>()) {
            child.applyClassRecursively(type, s);
        }
    } catch (_: Exception) {
        return;
    }
}