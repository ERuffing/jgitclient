package com.ethanruffing.ejgit.gui.view

import com.ethanruffing.ejgit.git.RepoAddType
import com.ethanruffing.ejgit.git.RepositoryAdder
import com.ethanruffing.ejgit.git.RepositoryManager
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.geometry.HPos
import javafx.scene.control.TabPane
import javafx.scene.layout.Priority
import javafx.scene.shape.StrokeType
import javafx.stage.DirectoryChooser
import org.eclipse.jgit.api.errors.GitAPIException
import tornadofx.*
import java.io.File
import java.util.logging.Level

class AddRepositoryDlg(val Manager: RepositoryManager) : View("Add Repository") {
    val RepoUrlProperty = SimpleStringProperty()
    var RepoUrl by RepoUrlProperty

    val DestinationProperty = SimpleStringProperty()
    var Destination by DestinationProperty
    val Adder = RepositoryAdder(Manager)

    override val root = vbox {
        tabpane {
            tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE
            tab("Clone") {
                onSelectionChanged = EventHandler {
                    Adder.repoAddType = RepoAddType.CLONE
                }
                anchorpane {
                    gridpane {
                        anchorpaneConstraints {
                            bottomAnchor = 0.0
                            leftAnchor = 0.0
                            rightAnchor = 0.0
                            topAnchor = 0.0
                        }

                        label("URL:") {
                            gridpaneConstraints {
                                marginBottom = 10.0
                                marginTop = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                        }
                        textfield {
                            promptText = "Remote repository URL"
                            gridpaneConstraints {
                                columnIndex = 1
                                columnSpan = 2147483647
                                marginBottom = 10.0
                                marginTop = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                            textProperty().bindBidirectional(RepoUrlProperty)
                        }

                        label("Destination:") {
                            gridpaneConstraints {
                                rowIndex = 1
                                marginBottom = 10.0
                                marginTop = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                        }
                        textfield {
                            promptText = "Destination folder path"
                            gridpaneConstraints {
                                columnIndex = 1
                                rowIndex = 1
                                marginBottom = 10.0
                                marginTop = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                            textProperty().bindBidirectional(DestinationProperty)
                        }
                        button("Browse") {
                            gridpaneConstraints {
                                columnIndex = 2
                                rowIndex = 1
                                marginBottom = 10.0
                                marginTop = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                        }.action {
                            browseForDirectory()
                        }

                        text() {
                            id = "validNewLocationText0"
                            strokeType = StrokeType.OUTSIDE
                            strokeWidth = 0.0
                            gridpaneConstraints {
                                columnIndex = 1
                                rowIndex = 2
                                marginBottom = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                            textProperty().bind(Adder.ValidNewLocationTextProperty)
                        }
                        checkbox("Requires login") {
                            isMnemonicParsing = false
                            gridpaneConstraints {
                                columnIndex = 0
                                columnSpan = 2
                                rowIndex = 3
                                hAlignment = HPos.LEFT
                                marginLeft = 15.0
                                marginRight = 15.0
                            }
                            selectedProperty().bindBidirectional(Adder.loginRequiredProperty)
                        }

                        label("Username:") {
                            gridpaneConstraints {
                                columnIndex = 0
                                rowIndex = 4
                                marginBottom = 5.0
                                marginLeft = 10.0
                                marginRight = 10.0
                                marginTop = 10.0
                            }
                        }
                        textfield {
                            gridpaneConstraints {
                                columnIndex = 1
                                rowIndex = 4
                                marginBottom = 5.0
                                marginLeft = 10.0
                                marginRight = 10.0
                                marginTop = 10.0
                            }
                            textProperty().bindBidirectional(Adder.usernameProperty)
                        }

                        label("Password:") {
                            gridpaneConstraints {
                                columnIndex = 0
                                rowIndex = 5
                                marginBottom = 5.0
                                marginLeft = 10.0
                                marginRight = 10.0
                                marginTop = 10.0
                            }
                        }
                        passwordfield {
                            gridpaneConstraints {
                                columnIndex = 1
                                rowIndex = 5
                                marginBottom = 5.0
                                marginLeft = 10.0
                                marginRight = 10.0
                                marginTop = 10.0
                            }
                            textProperty().bindBidirectional(Adder.passwordProperty)
                        }

                        constraintsForColumn(0).halignment = HPos.RIGHT
                        constraintsForColumn(0).hgrow = Priority.SOMETIMES
                        constraintsForColumn(0).minWidth = 10.0
                        constraintsForColumn(1).hgrow = Priority.ALWAYS
                        constraintsForColumn(1).minWidth = 10.0
                        constraintsForColumn(2).hgrow = Priority.SOMETIMES
                        constraintsForColumn(2).minWidth = 10.0
                        constraintsForRow(0).minHeight = 10.0
                        constraintsForRow(0).vgrow = Priority.SOMETIMES
                        constraintsForRow(1).vgrow = Priority.SOMETIMES
                        constraintsForRow(2).vgrow = Priority.ALWAYS
                        constraintsForRow(3).minHeight = 10.0
                        constraintsForRow(3).vgrow = Priority.SOMETIMES
                        constraintsForRow(4).minHeight = 10.0
                        constraintsForRow(4).vgrow = Priority.SOMETIMES
                        constraintsForRow(5).minHeight = 10.0
                        constraintsForRow(5).vgrow = Priority.SOMETIMES
                        constraintsForRow(6).minHeight = 10.0
                        constraintsForRow(6).prefHeight = 30.0
                        constraintsForRow(6).vgrow = Priority.ALWAYS
                    }
                }
            }
            tab("Add Working Copy") {
                onSelectionChanged = EventHandler {
                    Adder.repoAddType = RepoAddType.ADD
                }

                anchorpane {
                    minHeight = 0.0
                    minWidth = 0.0
                    prefHeight = 180.0
                    prefWidth = 200.0

                    gridpane {
                        prefHeight = 179.0
                        prefWidth = 424.0
                        anchorpaneConstraints {
                            bottomAnchor = 0.0
                            leftAnchor = 0.0
                            rightAnchor = 0.0
                            topAnchor = 0.0
                        }

                        label("Location:") {
                            gridpaneConstraints {
                                marginBottom = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                                marginTop = 10.0
                            }
                        }
                        textfield {
                            gridpaneConstraints {
                                columnIndex = 1
                                marginBottom = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                                marginTop = 10.0
                            }
                            textProperty().bindBidirectional(Adder.DestinationProperty)
                        }
                        button("Browse") {
                            gridpaneConstraints {
                                columnIndex = 2
                                hAlignment = HPos.CENTER
                                marginBottom = 10.0
                                marginTop = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                        }.action {
                            browseForDirectory()
                        }

                        text() {
                            strokeType = StrokeType.OUTSIDE
                            strokeWidth = 0.0
                            gridpaneConstraints {
                                columnIndex = 1
                                rowIndex = 1
                                marginBottom = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                            textProperty().bind(Adder.ValidNewLocationTextProperty)
                        }

                        constraintsForColumn(0).halignment = HPos.RIGHT
                        constraintsForColumn(0).hgrow = Priority.SOMETIMES
                        constraintsForColumn(1).hgrow = Priority.ALWAYS
                        constraintsForColumn(2).hgrow = Priority.SOMETIMES
                        constraintsForRow(0).minHeight = 10.0
                        constraintsForRow(0).vgrow = Priority.NEVER
                        constraintsForRow(1).minHeight = 10.0
                        constraintsForRow(1).vgrow = Priority.NEVER
                        constraintsForRow(2).minHeight = 10.0
                        constraintsForRow(2).prefHeight = 30.0
                        constraintsForRow(2).vgrow = Priority.SOMETIMES
                    }
                }
            }
            tab("Create New") {
                onSelectionChanged = EventHandler {
                    Adder.repoAddType = RepoAddType.NEW
                }

                anchorpane {
                    minHeight = 0.0
                    minWidth = 0.0
                    prefHeight = 180.0
                    prefWidth = 200.0

                    gridpane {
                        prefHeight = 179.0
                        prefWidth = 424.0
                        anchorpaneConstraints {
                            bottomAnchor = 0.0
                            leftAnchor = 0.0
                            rightAnchor = 0.0
                            topAnchor = 0.0
                        }

                        label("Location:") {
                            gridpaneConstraints {
                                marginBottom = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                                marginTop = 10.0
                            }
                        }
                        textfield {
                            gridpaneConstraints {
                                columnIndex = 1
                                marginBottom = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                                marginTop = 10.0
                            }
                            textProperty().bindBidirectional(Adder.DestinationProperty)
                        }
                        button("Browse") {
                            gridpaneConstraints {
                                columnIndex = 2
                                hAlignment = HPos.CENTER
                                marginBottom = 10.0
                                marginTop = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                        }.action {
                            browseForDirectory()
                        }

                        text() {
                            strokeType = StrokeType.OUTSIDE
                            strokeWidth = 0.0
                            gridpaneConstraints {
                                columnIndex = 1
                                rowIndex = 1
                                marginBottom = 10.0
                                marginLeft = 10.0
                                marginRight = 10.0
                            }
                            textProperty().bind(Adder.ValidNewLocationTextProperty)
                        }

                        constraintsForColumn(0).halignment = HPos.RIGHT
                        constraintsForColumn(0).hgrow = Priority.SOMETIMES
                        constraintsForColumn(1).hgrow = Priority.ALWAYS
                        constraintsForColumn(2).hgrow = Priority.SOMETIMES
                        constraintsForRow(0).minHeight = 10.0
                        constraintsForRow(0).vgrow = Priority.NEVER
                        constraintsForRow(1).minHeight = 10.0
                        constraintsForRow(1).vgrow = Priority.NEVER
                        constraintsForRow(2).minHeight = 10.0
                        constraintsForRow(2).prefHeight = 30.0
                        constraintsForRow(2).vgrow = Priority.SOMETIMES
                    }
                }
            }
        }

        gridpane {
            button("OK") {
                isDefaultButton = true
                prefWidth = 65.0
                gridpaneConstraints {
                    columnIndex = 1
                    marginBottom = 5.0
                    marginLeft = 5.0
                    marginRight = 5.0
                    marginTop = 5.0
                }
            }.action {
                try {
                    Adder.add();
                } catch (e: GitAPIException) {
                    log.log(Level.SEVERE, "Error adding repository.", e)
                }
                close()
            }
            button("Cancel") {
                isCancelButton = true
                prefWidth = 65.0
                gridpaneConstraints {
                    columnIndex = 2
                    marginBottom = 5.0
                    marginLeft = 5.0
                    marginRight = 5.0
                    marginTop = 5.0
                }
            }.action {
                close()
            }

            constraintsForColumn(0).hgrow = Priority.ALWAYS
            constraintsForColumn(0).minWidth = 10.0
            constraintsForColumn(0).prefWidth = 100.0
            constraintsForColumn(1).hgrow = Priority.SOMETIMES
            constraintsForColumn(1).minWidth = 10.0
            constraintsForColumn(2).hgrow = Priority.SOMETIMES
            constraintsForColumn(2).minWidth = 10.0
            constraintsForRow(0).minHeight = 10.0
            constraintsForRow(0).vgrow = Priority.SOMETIMES
        }
    }

    private fun browseForDirectory() {
        val chooser = DirectoryChooser()
        val f = File(Adder.Destination)
        if (f.exists() && f.isDirectory) {
            chooser.initialDirectory = f
        }
        val selected = chooser.showDialog(null)
        if (selected != null) Adder.Destination = selected.path
    }
}
