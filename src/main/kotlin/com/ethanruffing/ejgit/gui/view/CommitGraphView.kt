package com.ethanruffing.ejgit.gui.view

import com.ethanruffing.ejgit.git.RepositoryManager
import javafx.beans.property.ReadOnlyStringWrapper
import tornadofx.*
import org.eclipse.jgit.revwalk.RevCommit
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class CommitGraphView(val Manager: RepositoryManager) : View("Commit Graph") {
    override val root = tableview(Manager.CommitsProperty) {
//TODO: Add graph column
//            column("Graph" )
        column("Description", RevCommit::getShortMessage)
        column<RevCommit?, String>("Date") {
            var instant = Instant.ofEpochSecond(it.value?.commitTime?.toLong() ?: 0)
            var formatter = DateTimeFormatter.ofPattern("d MMM yyyy HH:mm").withZone(ZoneId.systemDefault());
            ReadOnlyStringWrapper(formatter.format(instant))
        }
        column<RevCommit?, String>("Author") {
            if (it.value == null)
                ReadOnlyStringWrapper("")
            else
                ReadOnlyStringWrapper(
                    it.value!!.authorIdent.name + "<" + it.value!!.authorIdent.emailAddress + ">"
                )
        }
        column<RevCommit?, String>("Commit"){
            ReadOnlyStringWrapper(it.value?.id?.name ?: "")
        }
    }

}
