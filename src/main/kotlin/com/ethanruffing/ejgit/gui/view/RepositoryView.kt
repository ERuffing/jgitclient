package com.ethanruffing.ejgit.gui.view

import com.ethanruffing.ejgit.git.RepositoryManager
import com.ethanruffing.ejgit.gui.applyClassRecursively
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TabPane
import tornadofx.*

class RepositoryView(val Manager: RepositoryManager) : View("Repository") {
    override val root = anchorpane {
        tabpane {
            anchorpaneConstraints {
                leftAnchor = 0.0
                topAnchor = 0.0
                rightAnchor = 0.0
                bottomAnchor = 0.0
            }
            tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE
            tab("Files") {
                id = "filesTab"
                add(StagingView(Manager))
            }
            tab("History") {
                add(CommitGraphView(Manager))
            }
        }

        applyClassRecursively(Button::class, "btn")
        applyClassRecursively(Label::class, "lbl")
//        applyClassRecursively(Pane::class, "panel")
    }
}