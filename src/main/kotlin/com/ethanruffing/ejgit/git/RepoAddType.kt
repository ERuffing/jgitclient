package com.ethanruffing.ejgit.git


/**
 * An enumeration of addition types, such as creating a new repository or
 * cloning a remote repository.
 */
enum class RepoAddType {
    /**
     * For cloning a remote repository.
     */
    CLONE,

    /**
     * For adding an existing working copy.
     */
    ADD,

    /**
     * For creating a new local repository.
     */
    NEW
}