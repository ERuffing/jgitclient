// TODO: Add a way to import and export preferences
package com.ethanruffing.ejgit.git

import com.ethanruffing.preferenceabstraction.AutoPreferences
import javafx.beans.property.*
import org.eclipse.jgit.revwalk.RevCommit
import java.lang.IndexOutOfBoundsException
import org.eclipse.jgit.api.errors.GitAPIException
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.CreateBranchCommand
import kotlin.jvm.JvmOverloads
import org.eclipse.jgit.api.ListBranchCommand
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import javafx.collections.FXCollections
import javafx.beans.value.ChangeListener
import org.apache.commons.configuration.ConfigurationException
import org.apache.commons.io.FileUtils
import org.eclipse.jgit.treewalk.CanonicalTreeParser
import org.eclipse.jgit.diff.DiffFormatter
import org.eclipse.jgit.treewalk.filter.PathFilter
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.treewalk.FileTreeIterator
import org.slf4j.LoggerFactory
import java.io.*
import java.util.function.Consumer
import tornadofx.getValue
import tornadofx.setValue

/**
 * A class for managing a set of [ Repositories][org.eclipse.jgit.lib.Repository].
 *
 * @author Ethan Ruffing
 * @since 2015-08-05
 */
class RepositoryManager() {
    private val logger = LoggerFactory.getLogger(javaClass)

    var prefs: AutoPreferences? = null

    /**
     * The repositories managed by the application.
     */
    val RepositoriesProperty = SimpleListProperty<Repository?>()
    var Repositories by RepositoriesProperty


    /**
     * The list of branches in the currently selected repository.
     */
    val BranchesProperty = SimpleListProperty<Ref?>()
    var Branches by BranchesProperty

    /**
     * The list of commits in the currently selected repository.
     */
    val CommitsProperty = SimpleListProperty<RevCommit?>()
    var Commits by CommitsProperty

    val UnstagedFilesProperty = SimpleListProperty<String?>()
    var UnstagedFiles by UnstagedFilesProperty

    /**
     * A list of the files that have changed and whose changes have been staged
     * for commit.
     */
    val StagedFilesProperty = SimpleListProperty<String?>()
    var StagedFiles by StagedFilesProperty

    /**
     * The text in [.DiffText], separated by line into a list.
     */
    val DiffTextListProperty = SimpleListProperty<String?>()
    var DiffTextList by DiffTextListProperty

    /**
     * The repository that is currently selected.
     */
    val SelectedRepositoryIndexProperty = object : SimpleIntegerProperty() {
        override fun set(value: Int) {
            super.set(value)
            refreshLists()
        }
    }
    var SelectedRepositoryIndex by SelectedRepositoryIndexProperty

    val SelectedRepositoryProperty = object : SimpleObjectProperty<Repository>() {
        override fun set(value: Repository?) {
            super.set(value);

            SelectedRepositoryIndex = if (value == null) Repositories.indexOf(value) else 0;
            refreshLists();
        }
    }

    /**
     * Gets the currently selected repository.
     *
     * @return The repository in [.repositoriesProperty] indicated by
     * [.selectedRepositoryIndexProperty].
     */
    var SelectedRepository by SelectedRepositoryProperty

    /**
     * The index of the unstaged file that is currently selected.
     */
    val SelectedUnstagedFileIndexProperty = object : SimpleIntegerProperty() {
        override fun set(value: Int) {
            if (value < 0 || value >= UnstagedFiles.size) {
                DiffText = ""
            } else {
                try {
                    val walk = RevWalk(SelectedRepository)
                    val commit = walk.parseCommit(
                        SelectedRepository!!.resolve("HEAD")
                    )
                    val tree = walk.parseTree(commit.tree.id)
                    val oldTreeParser = CanonicalTreeParser()

                    SelectedRepository!!.newObjectReader().use { oldReader -> oldTreeParser.reset(oldReader, tree.id) }
                    walk.dispose()
                    val newTreeParser = FileTreeIterator(
                        SelectedRepository
                    )
                    val out = ByteArrayOutputStream()
                    val formatter = DiffFormatter(out)
                    formatter.pathFilter = PathFilter.create(
                        UnstagedFiles[value]
                    )
                    formatter.setRepository(SelectedRepository)
                    val diff = formatter.scan(
                        oldTreeParser,
                        newTreeParser
                    )
                    for (entry: DiffEntry? in diff) {
                        out.reset()
                        formatter.format(entry)
                        DiffText = out.toString("UTF-8")
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }
    var SelectedUnstagedFileIndex by SelectedUnstagedFileIndexProperty

    /**
     * The index of the staged file that is currently selected.
     */
    val SelectedStagedFileIndexProperty = object : SimpleIntegerProperty() {
        override fun set(value: Int) {
            if (value < 0 || value >= StagedFiles.size) {
                DiffText = ""
            } else {
                try {
                    val walk = RevWalk(
                        SelectedRepository
                    )
                    val commit = walk.parseCommit(
                        SelectedRepository!!.resolve("HEAD")
                    )
                    val tree = walk.parseTree(commit.tree.id)
                    val oldTreeParser = CanonicalTreeParser()
                    SelectedRepository!!.newObjectReader()
                        .use { oldReader -> oldTreeParser.reset(oldReader, tree.getId()) }
                    walk.dispose()
                    val newTreeParser = FileTreeIterator(
                        SelectedRepository
                    )
                    val out = ByteArrayOutputStream()
                    val formatter = DiffFormatter(out)
                    formatter.pathFilter = PathFilter.create(
                        StagedFiles[value]
                    )
                    formatter.setRepository(SelectedRepository)
                    val diff = formatter.scan(
                        oldTreeParser,
                        newTreeParser
                    )
                    for (entry: DiffEntry? in diff) {
                        out.reset()
                        formatter.format(entry)
                        DiffText = out.toString("UTF-8")
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }
    var SelectedStagedFileIndex by SelectedStagedFileIndexProperty

    /**
     * The index of the currently selected branch within the [ ][.branchesProperty] list.
     */
    val SelectedBranchIndexProperty = SimpleIntegerProperty()
    var SelectedBranchIndex by SelectedBranchIndexProperty

    val SelectedBranchProperty = object : SimpleObjectProperty<Ref>() {
        override fun set(value: Ref) {
            super.set(value);
            SelectedBranchIndex = Branches.indexOf(value);
            refreshLists();
        }
    }
    var SelectedBranch by SelectedBranchProperty

    /**
     * The text representing a `diff` of the changes on the currently
     * selected file.
     */
    val DiffTextProperty = SimpleStringProperty()
    var DiffText by DiffTextProperty

    /**
     * The name of the active branch.
     *
     * @return The name of the active branch.
     * @throws IOException Thrown if an error occurs while retrieving the branch
     * name.
     */
    @get:Throws(IOException::class)
    val CurrentBranchName: String
        get() = SelectedRepository!!.branch

    /**
     * The branch that has currently been selected.
     *
     * @return The current branch.
     * @throws IndexOutOfBoundsException Thrown if the selection index is out of
     * range for the list of branches.
     */
    @get:Throws(IndexOutOfBoundsException::class)
    val SelectedBranchName: String
        get() = Branches[SelectedBranchIndex]?.name
            ?.replaceFirst("refs/heads/".toRegex(), "")
            ?.replaceFirst("refs/remotes/".toRegex(), "") ?: ""

    /**
     * Closes the repository handles and prepares to close the application.
     */
    fun close() {
        Repositories.forEach(Consumer { obj: Repository? -> obj!!.close() })
    }

    /**
     * Checks out the selected branch.
     *
     * @throws WorkingCopyNotCleanException Thrown if the working copy contains
     * uncommitted changes.
     */
    @Throws(com.ethanruffing.ejgit.git.WorkingCopyNotCleanException::class, GitAPIException::class)
    fun checkoutSelectedBranch() {
        // Ensure that the working copy is clean
        if (UnstagedFiles.size > 0 || StagedFiles.size > 0) throw com.ethanruffing.ejgit.git.WorkingCopyNotCleanException(
            (
                    "The working copy contains changes that have not been "
                            + "committed.")
        )

        // Check out the chosen branch
        val git = Git(SelectedRepository)
        val name = CurrentBranchName
        //TODO: Replace "origin" with something more generic
        if (name.contains("origin")) {
            git.checkout()
                .setCreateBranch(true)
                .setName(name.replace("refs/remotes/origin/", ""))
                .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK)
                .setStartPoint(name.replace("refs/remotes/", ""))
                .call()
            println(
                "Checked out new branch " + name.replace(
                    "refs/remotes/origin/",
                    ""
                ) + " to track remote " + name.replace("refs/remotes/", "")
            )
        } else {
            git.checkout().setName(name).call()
        }
        refreshLists()
    }

    /**
     * Commits the currently staged items.
     */
    @Throws(GitAPIException::class)
    fun commit(commitBuilder: CommitBuilder) {
        val git = Git(SelectedRepository)
        git.commit()
            .setAuthor(
                commitBuilder.authorName,
                commitBuilder.authorEmail
            )
            .setCommitter(
                commitBuilder.committerName,
                commitBuilder.committerEmail
            )
            .setMessage(commitBuilder.commitMessage)
            .setAmend(commitBuilder.amend)
            .call()
        refreshLists()
    }

    @JvmOverloads
    @Throws(GitAPIException::class)
    fun push(ref: String? = CurrentBranchName, remote: String? = "origin") {
        val git = Git(SelectedRepository)
        git.push().add(ref).setRemote(remote).call()
    }

    @Throws(GitAPIException::class)
    fun pull(ref: String? = CurrentBranchName, remote: String? = "origin") {
        val git = Git(SelectedRepository)
        git.pull().setRemote("origin").call()
    }

    @Throws(GitAPIException::class)
    fun fetch() {
        val git = Git(SelectedRepository)
        git.fetch().call()
    }

    /**
     * Stages all changes in a file.
     *
     * @param file The file to stage changes from.
     */
    @Throws(GitAPIException::class)
    fun stage(file: String?) {
        val git = Git(SelectedRepository)
        git.add().addFilepattern(file).call()
        refreshLists()
    }

    /**
     * Stages all changes in all files.
     */
    @Throws(GitAPIException::class)
    fun stageAll() {
        val git = Git(SelectedRepository)
        git.add().addFilepattern(".").call()
        refreshLists()
    }

    /**
     * Unstages all changes in a file.
     *
     * @param file The file to unstage changes from.
     */
    @Throws(GitAPIException::class)
    fun unstage(file: String?) {
        val git = Git(SelectedRepository)
        git.reset().setRef(Constants.HEAD).addPath(file).call()
        refreshLists()
    }

    /**
     * Unstages all changes in all files.
     *
     */
    @Throws(GitAPIException::class)
    fun unstageAll() {
        val git = Git(SelectedRepository)
        var reset = git.reset().setRef(Constants.HEAD)
        for (file in StagedFiles) {
            reset.addPath(file)
        }
        reset.call()
        refreshLists()
    }

    /**
     * Updates the various lists with current information.
     */
    fun refreshLists() {
        try {
            val git = Git(SelectedRepository)
            val call = git.branchList()
                .setListMode(ListBranchCommand.ListMode.ALL)
                .call()
            Branches.setAll(call)
            val commits = git.log().all().call()
            val toRemove: List<RevCommit?> = Commits
            Commits.removeAll(toRemove)
            commits.forEach(Consumer { commit: RevCommit? -> Commits.add(commit) })
            val status = git.status().call()
            UnstagedFiles.clear()
            status.missing.forEach(Consumer { f: String? -> UnstagedFiles.add(f) })
            status.modified.forEach(Consumer { f: String? -> UnstagedFiles.add(f) })
            status.untracked.forEach(Consumer { f: String? -> UnstagedFiles.add(f) })
            StagedFiles.clear()
            status.changed.forEach(Consumer { f: String? -> StagedFiles.add(f) })
            status.added.forEach(Consumer { f: String? -> StagedFiles.add(f) })
        } catch (e: GitAPIException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


    /**
     * Parses the string representation of the list of repositories and loads
     * them.
     *
     * @param pref The preference string to parse.
     * @throws IOException Thrown if an error occurs when loading the
     * repositories found in the given preferences line.
     */
    @Throws(IOException::class)
    private fun parseRepoPref(pref: String) {
        val repos = pref.split(";").toTypedArray()
        for (r: String in repos) {
            if (!r.isEmpty()) {
                val builder = FileRepositoryBuilder()
                val repository = builder.setGitDir(File(r))
                    .readEnvironment()
                    .findGitDir()
                    .build()
                Repositories.add(repository)
            }
        }
    }

    /**
     * Encodes a string representation of the list of repositories.
     *
     * @return The encoded string.
     */
    private fun encodeRepoPref(): String {
        var pref = ""
        for (r: Repository? in Repositories) {
            pref += r!!.directory.toString() + ";"
        }
        return pref
    }

    /**
     * Stores the repositories list to preferences.
     */
    fun storeRepoList() {
        prefs!!.put("repos", encodeRepoPref())
    }

    /**
     * Deletes the specified repository from the disk and removes it from the
     * list.
     *
     * @param index The index of the repository to delete.
     * @throws IOException Thrown if an error occurs while deleting the
     * repository.
     */
    @Throws(IOException::class)
    fun deleteRepository(index: Int) {
        if (index == SelectedRepositoryIndex) SelectedRepositoryIndex = if (index > 0) index - 1 else 0;
        FileUtils.deleteDirectory(Repositories[index]!!.workTree)
        Repositories[index]!!.close()
        Repositories.removeAt(index)
    }

    init {
        try {
            prefs = AutoPreferences(javaClass)
        } catch (e: ConfigurationException) {
            logger.error("Error loading preferences.", e)
        }
        val repoList = FXCollections.observableArrayList<Repository?>()
        Repositories = SimpleListProperty(repoList)
        parseRepoPref(prefs!!.getString("repos", ""))
        val branchesList = FXCollections.observableArrayList<Ref?>()
        Branches = SimpleListProperty(branchesList)
        val commitsList = FXCollections.observableArrayList<RevCommit?>()
        Commits = SimpleListProperty(commitsList)
        val unstagedFilesList = FXCollections.observableArrayList<String?>()
        UnstagedFiles = SimpleListProperty(unstagedFilesList)
        val stagedFilesList = FXCollections.observableArrayList<String?>()
        StagedFiles = SimpleListProperty(stagedFilesList)
        val diffTextListInternal = FXCollections.observableArrayList<String?>()
        DiffTextList = SimpleListProperty(diffTextListInternal)

        DiffTextProperty.addListener(ChangeListener { observable, oldValue, newValue ->
            try {
                val rdr = BufferedReader(
                    StringReader(
                        newValue
                    )
                )
                DiffTextList.clear()
                var line = rdr.readLine()
                while (line != null) {
                    DiffTextList.add(line)
                    line = rdr.readLine()
                }
                rdr.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
    }
}